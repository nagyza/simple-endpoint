package com.nagyza.simpleendpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleEndpointApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleEndpointApplication.class, args);
	}

}
