package com.nagyza.simpleendpoint.controller;


import com.nagyza.simpleendpoint.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainRestController {

    @GetMapping("/hello")
    public User hello(@RequestParam String name) {
        User user = new User(name);
        return user;
    }
}
